package ua.alxmute.sevices;

import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.ConversionService;
import org.springframework.stereotype.Service;
import ua.alxmute.data.access.dal.CustomerDal;
import ua.alxmute.data.access.domain.Customer;
import ua.alxmute.dto.CustomerCreateDto;
import ua.alxmute.dto.CustomerDto;
import ua.alxmute.exception.NotFoundException;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CustomerServiceImpl implements CustomerService {

    private final CustomerDal customerDal;
    private final ConversionService conversionService;

    @Override
    public CustomerDto save(CustomerCreateDto customerCreateDto) {
        Customer customer = conversionService.convert(customerCreateDto, Customer.class);
        return conversionService.convert(customerDal.save(customer), CustomerDto.class);
    }

    @Override
    public CustomerDto update(CustomerCreateDto customerCreateDto) {
        customerDal.findById(customerCreateDto.getId())
                .orElseThrow(() -> new NotFoundException("Customer not found"));
        Customer customer = conversionService.convert(customerCreateDto, Customer.class);
        return conversionService.convert(customerDal.save(customer), CustomerDto.class);
    }

    @Override
    public List<CustomerDto> findAll() {
        return customerDal.findAll()
                .stream()
                .map(customer -> conversionService.convert(customer, CustomerDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public CustomerDto findById(String id) {
        Customer customer = customerDal.findById(id)
                .orElseThrow(() -> new NotFoundException("Customer not found"));
        return conversionService.convert(customer, CustomerDto.class);
    }

    @Override
    public List<CustomerDto> findByCity(String city) {
        return customerDal.findByCity(city)
                .stream()
                .map(customer -> conversionService.convert(customer, CustomerDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public void deleteById(String id) {
        Customer customer = customerDal.findById(id)
                .orElseThrow(() -> new NotFoundException("Customer not found"));
        customerDal.delete(customer);
    }
}
