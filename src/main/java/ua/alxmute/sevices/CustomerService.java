package ua.alxmute.sevices;

import ua.alxmute.dto.CustomerCreateDto;
import ua.alxmute.dto.CustomerDto;

import java.util.List;

public interface CustomerService {

    CustomerDto save(CustomerCreateDto customerCreateDto);

    CustomerDto update(CustomerCreateDto customerCreateDto);

    List<CustomerDto> findAll();

    CustomerDto findById(String id);

    List<CustomerDto> findByCity(String city);

    void deleteById(String id);

}
