package ua.alxmute.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ua.alxmute.data.access.domain.Address;
import ua.alxmute.dto.AddressDto;

@Component
public class AddressToDtoConverter implements Converter<Address, AddressDto> {
    @Override
    public AddressDto convert(Address address) {
        return AddressDto.builder()
                .city(address.getCity())
                .zipCode(address.getZipCode())
                .build();
    }
}
