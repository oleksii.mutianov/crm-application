package ua.alxmute.converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ua.alxmute.data.access.domain.Address;
import ua.alxmute.dto.AddressCreateDto;

@Component
public class CreateDtoToAddressConverter implements Converter<AddressCreateDto, Address> {
    @Override
    public Address convert(AddressCreateDto addressCreateDto) {
        return Address.builder()
                .city(addressCreateDto.getCity())
                .zipCode(addressCreateDto.getZipCode())
                .build();
    }
}
