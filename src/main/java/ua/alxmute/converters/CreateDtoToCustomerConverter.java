package ua.alxmute.converters;

import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ua.alxmute.data.access.domain.Customer;
import ua.alxmute.dto.CustomerCreateDto;

@Component
@RequiredArgsConstructor
public class CreateDtoToCustomerConverter implements Converter<CustomerCreateDto, Customer> {

    private final CreateDtoToAddressConverter createDtoToAddressConverter;

    @Override
    public Customer convert(CustomerCreateDto customerCreateDto) {
        return Customer.builder()
                .id(customerCreateDto.getId())
                .firstName(customerCreateDto.getFirstName())
                .lastName(customerCreateDto.getLastName())
                .address(createDtoToAddressConverter.convert(customerCreateDto.getAddress()))
                .build();
    }
}
