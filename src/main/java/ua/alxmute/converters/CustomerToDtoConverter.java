package ua.alxmute.converters;

import lombok.RequiredArgsConstructor;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;
import ua.alxmute.data.access.domain.Customer;
import ua.alxmute.dto.CustomerDto;

@Component
@RequiredArgsConstructor
public class CustomerToDtoConverter implements Converter<Customer, CustomerDto> {

    private final AddressToDtoConverter addressToDtoConverter;

    @Override
    public CustomerDto convert(Customer customer) {
        return CustomerDto.builder()
                .id(customer.getId())
                .firstName(customer.getFirstName())
                .lastName(customer.getLastName())
                .address(addressToDtoConverter.convert(customer.getAddress()))
                .build();
    }
}
