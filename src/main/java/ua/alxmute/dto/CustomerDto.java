package ua.alxmute.dto;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class CustomerDto {

    private String id;

    private String firstName;

    private String lastName;

    private AddressDto address;

}
