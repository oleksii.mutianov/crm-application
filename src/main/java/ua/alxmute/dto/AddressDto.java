package ua.alxmute.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AddressDto {

    private String city;

    private String zipCode;

}
