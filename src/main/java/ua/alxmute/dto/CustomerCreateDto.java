package ua.alxmute.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CustomerCreateDto {

    private String id;

    private String firstName;

    private String lastName;

    private AddressCreateDto address;

}
