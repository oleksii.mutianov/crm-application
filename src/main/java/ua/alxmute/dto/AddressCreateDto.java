package ua.alxmute.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class AddressCreateDto {

    private String city;

    private String zipCode;

}
