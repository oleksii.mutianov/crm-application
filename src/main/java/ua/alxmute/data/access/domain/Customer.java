package ua.alxmute.data.access.domain;

import lombok.Builder;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@Document(collection = "customers")
public class Customer {

    @Id
    private String id;

    private String firstName;

    private String lastName;

    private Address address;
}
