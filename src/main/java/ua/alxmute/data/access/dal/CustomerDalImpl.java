package ua.alxmute.data.access.dal;

import lombok.RequiredArgsConstructor;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Repository;
import ua.alxmute.data.access.domain.Customer;

import java.util.List;
import java.util.Optional;

import static org.springframework.data.mongodb.core.query.Criteria.where;

@Repository
@RequiredArgsConstructor
public class CustomerDalImpl implements CustomerDal {

    private final MongoTemplate mongoTemplate;

    @Override
    public Customer save(Customer customer) {
        return mongoTemplate.save(customer);
    }

    @Override
    public List<Customer> findAll() {
        return mongoTemplate.findAll(Customer.class);
    }

    @Override
    public Optional<Customer> findById(String id) {
        return Optional.ofNullable(mongoTemplate.findById(id, Customer.class));
    }

    @Override
    public List<Customer> findByCity(String city) {
        Query query = new Query();
        query.addCriteria(where("address.city").is(city));
        return mongoTemplate.find(query, Customer.class);
    }

    @Override
    public void delete(Customer customer) {
        mongoTemplate.remove(customer);
    }

    @Override
    public void deleteById(String id) {
        Query query = new Query();
        query.addCriteria(where("id").is(id));
        mongoTemplate.remove(query, Customer.class);
    }
}
