package ua.alxmute.data.access.dal;

import ua.alxmute.data.access.domain.Customer;

import java.util.List;
import java.util.Optional;

public interface CustomerDal {

    Customer save(Customer customer);

    List<Customer> findAll();

    Optional<Customer> findById(String id);

    List<Customer> findByCity(String city);

    void delete(Customer customer);

    void deleteById(String id);

}
