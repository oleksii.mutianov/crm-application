package ua.alxmute.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ua.alxmute.dto.CustomerCreateDto;
import ua.alxmute.dto.CustomerDto;
import ua.alxmute.sevices.CustomerService;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("customers")
public class CustomerController {

    private final CustomerService customerService;

    @GetMapping
    public ResponseEntity<List<CustomerDto>> findAll() {
        return new ResponseEntity<>(customerService.findAll(), HttpStatus.OK);
    }

    @GetMapping("/city/{cityName}")
    public ResponseEntity<List<CustomerDto>> findByCity(@PathVariable("cityName") String cityName) {
        return new ResponseEntity<>(customerService.findByCity(cityName), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<CustomerDto> findById(@PathVariable("id") String id) {
        return new ResponseEntity<>(customerService.findById(id), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<CustomerDto> create(@RequestBody CustomerCreateDto customerCreateDto) {
        return new ResponseEntity<>(customerService.save(customerCreateDto), HttpStatus.CREATED);
    }
}
